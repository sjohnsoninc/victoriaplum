<?php

declare(strict_types = 1);

namespace VictoriaPlum\Abstracts;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use VictoriaPlum\Classes\Droid;
use VictoriaPlum\Classes\ResponseObject;
use VictoriaPlum\Classes\URLAssembler;
use VictoriaPlum\Classes\Map;
use VictoriaPlum\Exceptions\ThisShouldNeverHappenException;
use VictoriaPlum\Interfaces\PathFinderInterface;

abstract class AbstractPathFinder implements PathFinderInterface
{
    /**
     * Guzzle Client
     * @var Client
     */
    private Client $client;

    /**
     * @var URLAssembler
     */
    protected URLAssembler $URLAssembler;

    /**
     * Test constructor.
     * GuzzleClient is injected
     *
     * @param Client $client
     * @param URLAssembler $URLAssembler
     */
    public function __construct(Client $client, URLAssembler $URLAssembler)
    {
        $this->client = $client;
        $this->URLAssembler = $URLAssembler;
    }

    /**
     * @param Droid $droid
     * @param Map $map
     * @throws ThisShouldNeverHappenException
     */
    public function pathFind(Droid $droid, Map $map): void
    {
        throw new ThisShouldNeverHappenException;
    }

    /**
     * @param URLAssembler $URLAssembler
     * @return ResponseObject
     * @throws GuzzleException
     */
    protected function request(URLAssembler $URLAssembler): ResponseObject
    {
        $response = $this->client->get((string)$URLAssembler);
        return new ResponseObject($response);
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->URLAssembler->setPath($path);
    }

    /**
     * @param string $path
     * @return void
     * @throws GuzzleException
     */
    public function manual(string $path): void
    {
        $this->setPath($path);
        $response = $this->request($this->URLAssembler);

        $response = $response->getContents();
        echo $response->map . PHP_EOL . PHP_EOL;
        echo $response->message . PHP_EOL;

    }
}
