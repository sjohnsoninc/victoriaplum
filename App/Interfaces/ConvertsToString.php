<?php

namespace VictoriaPlum\Interfaces;

interface ConvertsToString
{
    /**
     * @return string
     */
    public function __toString(): string;
}
