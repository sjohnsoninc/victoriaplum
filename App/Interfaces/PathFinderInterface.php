<?php

namespace VictoriaPlum\Interfaces;

use GuzzleHttp\Client;
use VictoriaPlum\Classes\Droid;
use VictoriaPlum\Classes\URLAssembler;
use VictoriaPlum\Classes\Map;

interface PathFinderInterface
{
    /**
     * @param Client $client
     * @param URLAssembler $URLAssembler
     */
    public function __construct(Client $client, URLAssembler $URLAssembler);

    /**
     * @param Droid $droid
     * @param Map $map
     */
    public function pathFind(Droid $droid, Map $map): void;
}
