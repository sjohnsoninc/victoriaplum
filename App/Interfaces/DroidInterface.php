<?php

namespace VictoriaPlum\Interfaces;

interface DroidInterface
{
    public function __construct(string $path = '');

    public function moveForward();

    public function moveLeft();

    public function moveRight();
}
