<?php

namespace VictoriaPlum\Classes;

use VictoriaPlum\Interfaces\ConvertsToString;

class Empire implements ConvertsToString
{

    public function __toString(): string
    {
        return 'empire';
    }
}
