<?php

namespace VictoriaPlum\Classes;

use VictoriaPlum\Interfaces\ConvertsToString;

class URLAssembler implements ConvertsToString
{
    const BASE = 'https://deathstar.dev-tests.vp-ops.com/%s.php?name=%s&path=%s';

    /**
     * @var string
     */
    private string $name;

    /**
     * @var ConvertsToString
     */
    private ConvertsToString $side;

    /**
     * @var string
     */
    private string $path;

    /**
     * @param string $name
     * @param ConvertsToString $side
     */
    public function __construct(string $name, ConvertsToString $side)
    {
        $this->name = $name;
        $this->side = $side;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return sprintf(self::BASE, $this->side, $this->name, $this->path);
    }


}
