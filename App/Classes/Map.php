<?php

namespace VictoriaPlum\Classes;

use VictoriaPlum\Exceptions\ThisShouldNeverHappenException;

class Map
{
    const DROID = '*';
    const MOVE_RIGHT_OFFSET = 1;
    const MOVE_LEFT_OFFSET = -1;

    const MOVE_RIGHT = 'r';
    const MOVE_LEFT = 'l';

    /**
     * @var array
     */
    private array $map = [];

    /**
     * @var string
     */
    private string $path = '';

    /**
     * @var string
     */
    private string $lastMove;

    /**
     * @param string $row
     * @param string $move
     */
    public function addRow(string $row, string $move)
    {
        $this->map[] = $row;
        $this->path .= $move;
        $this->lastMove = $move;
    }

    private function undoLastMove(): void
    {
        if ($this->lastMove === 'f') {
            array_splice($this->map, -1, count($this->map) - 1);
            $this->path = substr($this->path, 0, -1);
        }
    }

    /**
     * @return string
     */
    public function getLastRow(): string
    {
        return $this->map[count($this->map) - 1];
    }

    /**
     * @param Droid $droid
     * @return string
     * @throws ThisShouldNeverHappenException
     */
    public function getSafeDirection(Droid $droid): string
    {
        $lastRow = $this->getLastRow();

        $droidPosition = $droid->getPositionY();

        $this->undoLastMove();

        for ($i = 1; $i < 9; $i++) {
            $offset = self::MOVE_RIGHT_OFFSET * $i;
            if ($lastRow[$droidPosition + $offset] === ' ') {
                return self::MOVE_RIGHT;
            }

            $offset = self::MOVE_LEFT_OFFSET * $i;
            if ($lastRow[$droidPosition + $offset] === ' ') {
                return self::MOVE_LEFT;
            }
        }

        throw new ThisShouldNeverHappenException;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }
}
