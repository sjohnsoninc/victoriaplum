<?php

namespace VictoriaPlum\Classes;

use VictoriaPlum\Interfaces\ConvertsToString;

class Alliance implements ConvertsToString
{

    public function __toString(): string
    {
        return 'alliance';
    }
}
