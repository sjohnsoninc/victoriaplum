<?php

namespace VictoriaPlum\Classes;

use Psr\Http\Message\ResponseInterface;

class ResponseObject
{
    private ResponseInterface $response;

    private object $contents;

    public function __construct(ResponseInterface $response)
    {
        $this->response = $response;
    }

    public function getStatusCode()
    {
        return $this->response->getStatusCode();
    }

    public function getContents()
    {
        if (isset($this->contents)) {
            return $this->contents;
        }

        $this->contents = json_decode($this->response->getBody()->getContents());
        return $this->contents;
    }
}
