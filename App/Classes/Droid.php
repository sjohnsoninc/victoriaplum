<?php

declare(strict_types = 1);

namespace VictoriaPlum\Classes;

use VictoriaPlum\Interfaces\DroidInterface;

class Droid implements DroidInterface
{
    const MOVE_LEFT_OFFSET = -1;
    const MOVE_RIGHT_OFFSET = 1;

    const MOVE_FORWARD = 'f';
    const MOVE_LEFT = 'l';
    const MOVE_RIGHT = 'r';

    /**
     * @var string the path the droid has taken
     */
    private string $path = '';

    /**
     * @var int droid's Y position
     */
    private int $positionY = 4;

    /**
     * @var int droid's X position
     */
    private int $positionX = 1;

    /**
     * @param string $path
     * @param int $positionY
     * @param int $positionX
     */
    public function __construct(string $path = 'f', int $positionY = 4, int $positionX = 1)
    {
        $this->path = $path;
        $this->positionY = $positionY;
        $this->positionX = $positionX;
    }

    /**
     * @return void
     */
    public function moveForward(): void
    {
        $this->path .= self::MOVE_FORWARD;
        $this->positionX += 1;
    }

    /**
     * @return void
     */
    public function moveLeft(): void
    {
        $this->path .= self::MOVE_LEFT;
        $this->positionY += self::MOVE_LEFT_OFFSET;
    }

    /**
     * @return void
     */
    public function moveRight(): void
    {
        $this->path .= self::MOVE_RIGHT;
        $this->positionY += self::MOVE_RIGHT_OFFSET;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getLastMove(): string
    {
        return $this->path[strlen($this->path) - 1];
    }

    /**
     * @return int
     */
    public function getPositionY(): int
    {
        return $this->positionY;
    }

    /**
     * @return int
     */
    public function getPositionX(): int
    {
        return $this->positionX;
    }

    /**
     * @param string $direction
     */
    public function moveDroid(string $direction): void
    {
        switch ($direction) {
            case self::MOVE_FORWARD:
                $this->moveForward();
                break;
            case self::MOVE_RIGHT:
                $this->moveRight();
                break;
            case self::MOVE_LEFT:
                $this->moveLeft();
                break;
        }
    }
}
