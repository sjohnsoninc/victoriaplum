<?php

namespace VictoriaPlum\Classes;

use GuzzleHttp\Exception\GuzzleException;
use VictoriaPlum\Abstracts\AbstractPathFinder;
use VictoriaPlum\Exceptions\ThisShouldNeverHappenException;
use VictoriaPlum\Interfaces\PathFinderInterface;

final class Pathfinder extends AbstractPathFinder implements PathFinderInterface
{
    /**
     * @param Droid $droid
     * @param Map $map
     * @throws ThisShouldNeverHappenException
     * @throws GuzzleException
     */
    public function pathFind(Droid $droid, Map $map): void
    {
        $path = $droid->getPath();
        $this->URLAssembler->setPath($path);

        $response = $this->request($this->URLAssembler);

        $statusCode = $response->getStatusCode();

        $returnedMap = explode(PHP_EOL, $response->getContents()->map);
        $map->addRow($returnedMap[count($returnedMap) - 1], $droid->getLastMove());

        if ($statusCode === 410) {
            /**
             * 410 - Lost contact.
             * Print the line and simply move forward.
             */
            echo $map->getLastRow() . PHP_EOL;
            $droid->moveForward();


            $this->pathFind($droid, $map);
        } elseif ($statusCode === 417) {
            /**
             * 417 - Crashed
             * Figure out a safe direction and proceed with it.
             */

            $safeDirection = $map->getSafeDirection($droid);
            $pathBeforeWeCrashed = $map->getPath();

            $positionX = $droid->getPositionX();

            if ($droid->getLastMove() === 'f') {
                $positionX = $positionX - 1;
            }

            // Since our droid crashed, we'll need a new one
            $droid = new Droid($pathBeforeWeCrashed, $droid->getPositionY(), $positionX);
            $droid->moveDroid($safeDirection);

            $this->pathFind($droid, $map);
        } elseif ($statusCode === 200) {
            /**
             * 200 - Success
             * Echo the final line and echo the path string.
             */

            echo $map->getLastRow() . PHP_EOL;
            echo PHP_EOL;
            echo $response->getContents()->message . PHP_EOL . PHP_EOL;

            // Not quite sure why simply echoing the string isn't considered a valid solution
            // I could create a fancy return object, and then manipulate that one where we'd actually be outputting the path
            // However considering that I'm printing every step, I'd say this should be acceptable, as the script is full with echos
            // It is a command line application after all
            echo 'Final droid position: [' . $droid->getPositionX() . ',' . $droid->getPositionY() . ']' . PHP_EOL;
            echo 'Path found:' . PHP_EOL;
            echo $droid->getPath() . PHP_EOL;
        }
    }


}
