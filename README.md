### Requirements
- php7.4
- composer

### Running the thing
First run `composer install`, this is required as the solution uses Guzzle library for making http requests and autoloading.

`php index.php` should start finding the path automatically. The script will print progress as it goes.
`php index.php path` can be used to verify a path manually. No automated pathfinding here.

