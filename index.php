<?php

/**
 * After running `composer install`
 * `php index.php` should just work, provided you've got PHP 7.4 installed
 */

require_once __DIR__ . '/vendor/autoload.php';

use \VictoriaPlum\Classes\Pathfinder;
use \VictoriaPlum\Classes\Alliance;
use \VictoriaPlum\Classes\Name;
use \VictoriaPlum\Classes\URLAssembler;
use \VictoriaPlum\Classes\Map;
use GuzzleHttp\Client;

$guzzleOptions = [
    'http_errors' => false,
];

$map = new Map;
$URLAssembler = new URLAssembler(new Name('Sven'), new Alliance);
$pathFinder = new Pathfinder(new Client($guzzleOptions), $URLAssembler);

if (isset($argv[1])) {
    $pathFinder->manual($argv[1]);
} else {
    //$pathFinder->pathFind($map);
    $pathFinder->pathFind(new \VictoriaPlum\Classes\Droid(), $map);
}
